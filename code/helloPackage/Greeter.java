package helloPackage;
import java.util.Scanner;
import java.util.Random;
import secondPackage.Utilities;
public class Greeter {
public static void main(String[] args){
    Random rand = new Random();
    Scanner reader = new Scanner(System.in);
    System.out.println("Please enter a number?");
    int num = reader.nextInt();
    num = Utilities.doubleMe(num);
    System.out.println("Your new number is " + num + ".");
}

}
